# Retro Blocks

## This mod is still under development and is not survival ready!

When finished, the mod will allow you to craft a time machine (name might change).
It works like a furnace, but its fuel is time... or more specifically clocks.

Blocks you put in it will be sent back in time, and will become 'Retro'. They behave exactly like the originals, but have old textures from Beta 1.3 days.

**Currently implemented:**
- Retro Blocks (cobblestone, mossy cobblestone, bricks, diamond, gold, iron, lapis blocks, and gravel)