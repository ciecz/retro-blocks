package ciecz.retroblocks.data;

import net.fabricmc.fabric.api.datagen.v1.DataGeneratorEntrypoint;
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;

public class RbDataGen implements DataGeneratorEntrypoint {

   @Override
   public void onInitializeDataGenerator(FabricDataGenerator fabricDataGenerator) {
      fabricDataGenerator.createPack().addProvider(RbLangEnUsProvider::new);
   }

}
