package ciecz.retroblocks.data;

import ciecz.retroblocks.inventory.RbCreativeTabs;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricLanguageProvider;

import static ciecz.retroblocks.block.RbBlocks.*;

public class RbLangEnUsProvider extends FabricLanguageProvider {

   protected RbLangEnUsProvider(FabricDataOutput dataOutput) {
      super(dataOutput, "en_us");
   }


   @Override
   public void generateTranslations(TranslationBuilder translationBuilder) {
      translationBuilder.add(RETRO_COBBLESTONE, "Retro Cobblestone");
      translationBuilder.add(RETRO_MOSSY_COBBLESTONE, "Retro Mossy Cobblestone");
      translationBuilder.add(RETRO_BRICKS, "Retro Bricks");
      translationBuilder.add(RETRO_DIAMOND_BLOCK, "Retro Block of Diamond");
      translationBuilder.add(RETRO_GOLD_BLOCK, "Retro Block of Gold");
      translationBuilder.add(RETRO_IRON_BLOCK, "Retro Block of Iron");
      translationBuilder.add(RETRO_LAPIS_BLOCK, "Retro Block of Lapis Lazuli");
      translationBuilder.add(RETRO_GRAVEL, "Retro Gravel");
      translationBuilder.add(RbCreativeTabs.CREATIVE_TAB, "Retro Blocks");

   }

}
