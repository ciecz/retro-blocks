package ciecz.retroblocks.block;

import ciecz.retroblocks.RbMod;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.FallingBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.List;

public class RbBlocks {

   public static List<Block> BLOCK_LIST = new ArrayList<>();

   // retro blocks
   public static final Block RETRO_COBBLESTONE = new Block(FabricBlockSettings
         .copyOf(Blocks.COBBLESTONE));
   public static final Block RETRO_MOSSY_COBBLESTONE = new Block(FabricBlockSettings
         .copyOf(Blocks.MOSSY_COBBLESTONE));
   public static final Block RETRO_BRICKS = new Block(FabricBlockSettings
         .copyOf(Blocks.BRICKS));
   public static final Block RETRO_DIAMOND_BLOCK = new Block(FabricBlockSettings
         .copyOf(Blocks.DIAMOND_BLOCK));
   public static final Block RETRO_GOLD_BLOCK = new Block(FabricBlockSettings
         .copyOf(Blocks.GOLD_BLOCK));
   public static final Block RETRO_IRON_BLOCK = new Block(FabricBlockSettings
         .copyOf(Blocks.IRON_BLOCK));
   public static final Block RETRO_LAPIS_BLOCK = new Block(FabricBlockSettings
         .copyOf(Blocks.LAPIS_BLOCK));
   public static final Block RETRO_GRAVEL = new FallingBlock(FabricBlockSettings
         .copyOf(Blocks.GRAVEL));


   /**
    * Adds block to block registry, creates BlockItem and adds it to item registry.
    */
   private static void registerBlock(String id, Block block) {
      Registry.register(Registries.BLOCK, new Identifier(RbMod.MOD_ID, id), block);
      Registry.register(Registries.ITEM, new Identifier(RbMod.MOD_ID, id),
            new BlockItem(block, new FabricItemSettings()));
      BLOCK_LIST.add(block);
   }


   public static void registerBlocks() {
      registerBlock("retro_cobblestone", RETRO_COBBLESTONE);
      registerBlock("retro_mossy_cobblestone", RETRO_MOSSY_COBBLESTONE);
      registerBlock("retro_bricks", RETRO_BRICKS);
      registerBlock("retro_diamond_block", RETRO_DIAMOND_BLOCK);
      registerBlock("retro_gold_block", RETRO_GOLD_BLOCK);
      registerBlock("retro_iron_block", RETRO_IRON_BLOCK);
      registerBlock("retro_lapis_block", RETRO_LAPIS_BLOCK);
      registerBlock("retro_gravel", RETRO_GRAVEL);
   }
}
