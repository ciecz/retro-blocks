package ciecz.retroblocks;

import ciecz.retroblocks.block.RbBlocks;
import ciecz.retroblocks.inventory.RbCreativeTabs;
import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RbMod implements ModInitializer {

   public static final String MOD_ID = "retroblocks";
   public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);


   @Override
   public void onInitialize() {
      LOGGER.info("Retro Blocks starting the init process...");
      RbCreativeTabs.registerCreativeTabs();
      RbBlocks.registerBlocks();
   }

}