package ciecz.retroblocks.inventory;

import ciecz.retroblocks.RbMod;
import ciecz.retroblocks.block.RbBlocks;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class RbCreativeTabs {

   public static ItemGroup CREATIVE_TAB
         = FabricItemGroup.builder(new Identifier(RbMod.MOD_ID, "blocks"))
         .icon(() -> new ItemStack(RbBlocks.RETRO_COBBLESTONE))
         .build();


   public static void registerCreativeTabs() {
      // Add all blocks and items to the tab
      ItemGroupEvents.modifyEntriesEvent(CREATIVE_TAB)
            .register(content -> RbBlocks.BLOCK_LIST.forEach(content::add));
   }
}
